const mongoosedb = require('mongoose');
const SchemaTipoPersonal =  new mongoosedb.Schema ({
    
    Tipo:{
        type:String ,
        unique:true
    },
    IdT:{
        type:Number ,
        unique:true
    }
  });
  const TipoPersonaldorModel = mongoosedb.model('tipo_personas1', SchemaTipoPersonal); 
  module.exports = TipoPersonaldorModel;
const mongoosedb = require('mongoose');
const SchemaPersonal =  new mongoosedb.Schema ({
    Nombre:{
        type:String 
    },
    Usuario: {
        type:String,
        unique:true
    },
    Contra: {
        type:String     
    },
    Tipo_Personal:{
        type:Number
    },
    Tipo_Desarrollador:{
        type:Number
    },
    Sesion:{
        type:Boolean
    }
  });
  const PersonalModel = mongoosedb.model('personal', SchemaPersonal); 
  module.exports = PersonalModel;
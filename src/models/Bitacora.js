const mongoosedb = require('mongoose');
const SchemaBitacora =  new mongoosedb.Schema ({
    Usuario_Cliente:{
        type:String 
    },
    Usuario_Desarrollador: {
        type:String
    },
    Bitacora: {
        type:String     
    }
  });
  const BitacoraModel = mongoosedb.model('bitacora', SchemaBitacora); 
  module.exports = BitacoraModel;
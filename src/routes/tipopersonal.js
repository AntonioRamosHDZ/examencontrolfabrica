const express = require("express");
const TPSchema = require('../models/Tipopersonales')
const sesionSchema = require("../models/sesion")
const router = express.Router();


router.post('/TP',(req,res)=>{
    // res.send("Usuario creado")
        const nuevotipo={
            "IdT" : req.body.IdT,
            "Tipo": req.body.Tipo
           
        }

        sesionSchema
        .findOne({ Sesion: 1 })
        .then((data) => {
            const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
            const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
            // console.log("Sesion: ", VSesion)
            // console.log("Tipo: ", VTipo)
            console.log(req.body.IdT)
            if (VTipo == 1 || VTipo == 2) {

                try {
                    const Tipo1 = TPSchema(nuevotipo);
                    console.log(nuevotipo)
                    
                    Tipo1
                    .save()
                    .then((data)=>{
                        res.json({mensaje:"ingresado"})
                    }).catch((error)=>{
                        console.log(error)
                        res.json({mensaje: "duplicado"})
                    })
    

                } catch (error) {
                    console.log(error)
                    res.json({ Mensaje: "error" })
                }
            }
            else {
                res.json({ Mensaje: "No tienes acceso a esta acción" })
            }

        })
        .catch((error) => {
            res.json({ mensaje: "Sesion no iniciada" })
        })


})


router.get('/TP', async (req, res) => {

    const Tipo1 = await TPSchema.find()
    sesionSchema
        .findOne({ Sesion: 1 })
        .then((data) => {
            const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
            const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
            console.log("Sesion: ", VSesion)
            console.log("Tipo: ", VTipo)

            if (VTipo == 1 || VTipo == 2) {

                try {

                    res.json(Tipo1)
                } catch (error) {
                    console.log(error)
                    res.json({ Mensaje: "error" })
                }
            }
            else {
                res.json({ Mensaje: "No tienes acceso a esta acción" })
            }
        })
        .catch((error) => {
            res.json({ mensaje: "Sesion no iniciada" })
        })
})


   


router.put('/TP', (req,res)=>{
   

    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]


        if (VTipo == 1 || VTipo == 2) {

            try {
                console.log(req.body.Tipo)
                console.log(req.body.IdT)
                TPSchema
                    .findOneAndUpdate({IdT: req.body.IdT}, {Tipo: req.body.Tipo})
                    .then((data) => {
                        console.log(data)
                        if(data==null){
                            res.json({ Mensaje: "Tipo no encontrado" })
                        }
                        else{
                            res.json({ Mensaje: "Tipo editada con exito" })
                        }
                        
                    }).catch((error) => {
                        console.log(error)
                        res.json({ Mensaje: "Error" })
                    })
                

            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "Tipo no encontrado" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }

    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })


})



router.delete('/TP',(req,res)=>{
    // res.send("Usuario Eliminado")
     
   
    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
    
        
        if (VTipo == 1) {

            try {
                TPSchema
                .findOneAndDelete({IdT: req.body.IdT})
                .then((data)=>{
                    if(data==null){
                        res.json({ Mensaje: "Tipo no encontrado" })
                    }
                    else{
                        res.json({ Mensaje: "Tipo eliminada con exito" })
                    }
                }).catch((error)=>{
                    console.log(error)
                    res.json({Mensaje:"Error1"})
                })

            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "Error2" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }
    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })
})




///////////////////////////////////////////////////////////////////////////////////////////////////////



module.exports = router;
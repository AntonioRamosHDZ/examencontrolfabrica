
const express = require("express");

const sesionSchema = require("../models/sesion")
const personalSchema = require('../models/Personal')

const router = express.Router();

// var Sesion = {"IniSesion" : 0};

router.post('/personal',(req,res)=>{
    // res.send("Usuario creado")

        const nuevopersonal={
            "Nombre" : req.body.Nombre,
            "Usuario": req.body.Usuario,  
            "Contra": req.body.Contra,
            "Tipo_Personal": req.body.Tipo_Personal,
            "Tipo_Desarrollador": req.body.Tipo_Desarrollador,
            "Sesion": req.body.Sesion,
        }
        ContraA = parseInt(req.body.ContraADM)

        Usuario1 = String(req.body.Nombre)
        Contra1 = String(req.body.Nombre)
        Tipo_Personal1 = String(req.body.Nombre)
        Tipo_Desarrollador1 = String(req.body.Nombre)
        Sesion=String(req.body.Sesion)
        console.log(Sesion)
        

        //Agregar por primera vez Admi principal
        if(ContraA == 12345 || ContraA == 98765 ){
            const personal = personalSchema(nuevopersonal);
            console.log(personal)
            personal
            .save()
            .then((data)=>{
                res.json({mensaje:"Ingresado"})
            }).catch((error)=>{
                res.json({mensaje: "Usuario duplicado"})
            })
        }
        else{
  
            res.json({mensaje: "Contraseña invalida"})
        }

})


//Agregar personal con un admi o cliente en la base de datos
router.post('/personal/Agregar', (req, res) => {
    // res.send("Usuario creado")

    const nuevopersonal = {
        "Nombre": req.body.Nombre,
        "Usuario": req.body.Usuario,
        "Contra": req.body.Contra,
        "Tipo_Personal": req.body.Tipo_Personal,
        "Tipo_Desarrollador": req.body.Tipo_Desarrollador,
        "Sesion": 0,
    }

    sesionSchema
        .findOne({ Sesion: 1 })
        .then((data) => {
            const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
            const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
            console.log("Sesion: ", VSesion)
            console.log("Tipo: ", VTipo)

            if (VTipo == 1 || VTipo == 2) {

                try {
                   
                    const personal = personalSchema(nuevopersonal);
                    personal
                        .save()
                        .then((data) => {
                            res.json({ Mensaje: "Personal agregado con exito" })
                        }).catch((error) => {
                            res.json({ Mensaje: "Usuario duplicado" })
                        })
                    

                } catch (error) {
                    console.log(error)
                    res.json({ Mensaje: "Error" })
                }
            }
            else {
                res.json({ Mensaje: "No tienes acceso a esta acción" })
            }

        })
        .catch((error) => {
            res.json({ mensaje: "Sesion no iniciada" })
        })


})



router.get('/personal', async (req, res) => {

    
    const personal = await personalSchema.find()
    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
        console.log("Sesion: ", VSesion)
        console.log("Tipo: ", VTipo)

        if (VTipo == 1 || VTipo == 2) {

            try {
               
                res.json(personal)
            
            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "error" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }

    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })


})


router.put('/personal', (req,res)=>{
   
    const nuevopersonal={
        "Nombre" : req.body.Nombre, 
        "Contra": req.body.Contra,
        "Tipo_Personal": req.body.Tipo_Personal,
        "Tipo_Desarrollador": req.body.Tipo_Desarrollador,
        "Sesion": 0,
    }
    
    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
        console.log("Sesion: ", VSesion)
        console.log("Tipo: ", VTipo)

        if (VTipo == 1 || VTipo == 2) {

            try {
                let  NewP = Editar({Usuario: req.body.Usuario}, nuevopersonal)
                res.json({ Mensaje: "Personal editado con exito" })

            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "Usuario no encontrado" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }

    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })


})



router.delete('/personal',(req,res)=>{
    // res.send("Usuario Eliminado")
     

    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
        console.log("Sesion: ", VSesion)
        console.log("Tipo: ", VTipo)
        
        if (VTipo == 1) {

            try {
                console.log({Usuario: req.body.Usuario})
                personalSchema
                    .findOneAndDelete({Usuario: req.body.Usuario})
                    .then((data) => {
                        res.json({ Mensaje: "Usuario eliminado" })
                        console.log("Usuario eliminado")
                    }).catch((error) => {
                        console.log(error)
                        res.json({ Mensaje: "Usuario no encontrado" })
                    })

            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "Error" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }
    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })
})

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
router.post('/personal/login',(req,res)=>{
    // res.send("Usuario creado")

        const nuevopersonal={
            "Usuario": req.body.Usuario,  
            "Contra": req.body.Contra,
        }
   
        UsuarioNew1=String(req.body.Usuario)
        // console.log(UsuarioNew1)
        personalSchema
        .findOne({Usuario : UsuarioNew1})
        .then((data)=>{
            const Datos = JSON.stringify(data)
            const VTpersonal = JSON.stringify({Tipo_Personal: data.Tipo_Personal}).split(":")[1].split("}")[0]
            const VSesion = JSON.stringify({TSesion: data.Sesion}).split(":")[1].split("}")[0]

             console.log(VTpersonal)
             console.log(VSesion)

            //iniciar sesion
            if (VSesion == "false")
            {
               let activar = Editar({"Usuario" : UsuarioNew1},{"Sesion": "true"}) //(buscar,editar)
               let Gsesion= GuardarSesion(VTpersonal)
                res.json({Mensaje:"Sesion iniciada correctamente"})
                
            }
            //Cerrar sesion
            else{
                let Apagar = Editar({"Usuario" : UsuarioNew1},{"Sesion": "false"}) //(buscar,editar)
                let Apagarsesion = CerrarSesion();

                res.json({Mensaje:"La sesion se cerro"})
 
            }
        })


        .catch((error)=>{
            console.log(error)
            res.json({Mensaje:"Usuario no encontrado"})
        })
    
})



router.get('/personal/ver', async (req, res) => {
        try {
            const sesion = await sesionSchema.find()
            res.json(sesion)
            
        } catch (error) {
            console.log(error)
        }

})

///////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////


function GuardarSesion(tipo) {
    try {

        const nuevo = {
            "Sesion": 1,
            "Tipo_Personal": tipo,
        }

        const newsesion = sesionSchema(nuevo);
        newsesion
            .save()
            .then((data) => {
                console.log("ingresado")
                console.log("//////////////////////////////////")

            }).catch((error) => {
                console.log("error", error)
            })

    } catch (error) {
        console.log(error)

    }

}

function CerrarSesion() {
    try {
        sesionSchema
            .deleteOne()
            .then((data) => {
                console.log("Sesion cerrada")
            
            }).catch((error) => {
                console.log("error", error)
            })
    } catch (error) {
        console.log(error)

    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////


function  BuscarAdmi(ID)
{
    console.log(ID)
    personalSchema
    .findOne({_id:ID})
    .then((info)=>{
      
        Tipo =JSON.stringify(info).split(",")[4].split(":")[1]
        console.log("////////////////////////")
        
        return Tipo;
    }).catch((error)=>{
        console.log(error)
        return 0;
    })
}


function Agregar(nuevo) {
    try {
        console.log(nuevo)
        const personal = personalSchema(nuevo);
        personal
            .save()
            .then((data) => {
                
            }).catch((error) => {
               
            })
    } catch (error) {
        console.log(error)
        
    }
};


function Editar(buscar,cambiar){
    console.log(buscar)
    console.log(cambiar)
    personalSchema
    .findOneAndUpdate(buscar,cambiar)
    .then((data) => {
        console.log("Usuario Cambiado")
    }).catch((error) => {
        console.log(error)
    })

}


module.exports = {
   
    router:router
}
// module.exports = router;

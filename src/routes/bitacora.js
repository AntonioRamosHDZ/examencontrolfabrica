const express = require("express");
const BSchema = require('../models/Bitacora')
const sesionSchema = require("../models/sesion")
const router = express.Router();

// import { Tipo_Personal } from "./personal";


router.post('/Bitacora',(req,res)=>{

        const nuevoBitacora={
            "Usuario_Cliente" : req.body.UsuarioC,
            "Usuario_Desarrollador" : req.body.UsuarioD,
            "Bitacora" : req.body.Bitacora
        }
          
        sesionSchema
        .findOne({ Sesion: 1 })
        .then((data) => {
            const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
            const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
            // console.log("Sesion: ", VSesion)
            // console.log("Tipo: ", VTipo)
            console.log(req.body.IdT)
            if (VTipo == 1 || VTipo == 2) {

                try {
                    const BS = BSchema(nuevoBitacora);
                    console.log(BS)
                    
                    BS
                    .save()
                    .then((data)=>{
                        res.json({mensaje:"ingresado"})
                    }).catch((error)=>{
                        console.log(error)
                        res.json({mensaje: "duplicado"})
                    })
    

                } catch (error) {
                    console.log(error)
                    res.json({ Mensaje: "error" })
                }
            }
            else {
                res.json({ Mensaje: "No tienes acceso a esta acción" })
            }

        })
        .catch((error) => {
            res.json({ mensaje: "Sesion no iniciada" })
        })


})
        
        




router.get('/Bitacora', async (req, res) => {

            
    const BS = await BSchema.find()
    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
        console.log("Sesion: ", VSesion)
        console.log("Tipo: ", VTipo)

        if (VTipo == 1 || VTipo == 2) {

            try {
               
                res.json(BS)
            
            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "error" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }

    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })


})

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


router.put('/Bitacora', (req,res)=>{
   
    const nuevoBitacora={
        "Usuario_Cliente" : req.body.UsuarioC,
        "Usuario_Desarrollador" : req.body.UsuarioD,
        "Bitacora" : req.body.Bitacora
    }
      
    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
        // console.log("Sesion: ", VSesion)
        // console.log("Tipo: ", VTipo)
        console.log(req.body.IdT)
        if (VTipo == 1 || VTipo == 2) {

            try {
            
                console.log(BS)
                
                BSchema
                .findOneAndUpdate({Id: req.body.Id}, nuevoBitacora)
                .then((data)=>{
                    res.json({mensaje:"ingresado"})
                }).catch((error)=>{
                    console.log(error)
                    res.json({mensaje: "duplicado"})
                })


            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "error" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }

    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })


})


router.delete('/Bitacora',(req,res)=>{
    // res.send("Usuario Eliminado")
     
 
    sesionSchema
    .findOne({ Sesion: 1 })
    .then((data) => {
        const VSesion = JSON.stringify({ Sesion: data.Sesion }).split(":")[1].split("}")[0]
        const VTipo = JSON.stringify({ Tipo_Personal: data.Tipo_Personal }).split(":")[1].split("}")[0]
    
        
        if (VTipo == 1) {

            try {
                BSchema
                .findOneAndDelete({Id: req.body.Id})
                .then((data)=>{
                    if(data==null){
                        res.json({ Mensaje: "Bitacora no encontrado" })
                    }
                    else{
                        res.json({ Mensaje: "Bitacora eliminada con exito" })
                    }
                }).catch((error)=>{
                    console.log(error)
                    res.json({Mensaje:"Error1"})
                })

            } catch (error) {
                console.log(error)
                res.json({ Mensaje: "Error2" })
            }
        }
        else {
            res.json({ Mensaje: "No tienes acceso a esta acción" })
        }
    })
    .catch((error) => {
        res.json({ mensaje: "Sesion no iniciada" })
    })
})



module.exports = router;